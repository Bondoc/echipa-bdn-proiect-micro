# Echipa-BDN-Proiect-Micro



## Getting started

Deschideti proiectul Keil uVision, compilati si incarcati pe placuta FRDM KL25Z binarul.
Deschidezi sursa ceva.py realizati toate importurile iar apoi rulati sursa. 
In terminal vor fi afisate porturile COM disponibile. Selectati portul placutei.
Mai departe va fi cerut baud rate ul. In cod este setat 115200. Daca nu l-ati schimbat, il folositi pe acesta.
Dupa aceasta se va deschide fereastra aplicatiei si totul va rula.
Din motive de sincronizare este necesar sa apasati butonul reset de pe placuta dupa ce fereastra aplicatiei se deschide.

Pentru a functiona trebuie conectat un flame sensor pe portul B pinul 0 -> canalul 8 si un sound sensor pe portul B pinul 1 -> canalul 9