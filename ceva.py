from PyQt5 import QtWidgets, QtCore, QtGui
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg
import sys  # We need sys so that we can pass argv to QApplication
import os
from random import randint
import serial.tools.list_ports
from os import environ

def suppress_qt_warnings():
    environ["QT_DEVICE_PIXEL_RATIO"] = "0"
    environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    environ["QT_SCREEN_SCALE_FACTORS"] = "1"
    environ["QT_SCALE_FACTOR"] = "1"

class _Bar(QtWidgets.QWidget):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setSizePolicy(
            QtWidgets.QSizePolicy.MinimumExpanding,
            QtWidgets.QSizePolicy.MinimumExpanding
        )
        self.gf = [0]
        self.vm = 330

    def sizeHint(self):
        return QtCore.QSize(40,120)

    def paintEvent(self, e):
        painter = QtGui.QPainter(self)

        brush = QtGui.QBrush()
        brush.setColor(QtGui.QColor('black'))
        brush.setStyle(QtCore.Qt.SolidPattern)
        rect = QtCore.QRect(0, 0, painter.device().width(), painter.device().height())
        painter.fillRect(rect, brush)

        # Get current state.
        #dial = self.parent()._dial
    ###################
        vmin = 0
        vmax = self.vm
        self.gf.reverse()
        value = self.gf[0] * 100
        self.gf.reverse()
    #####################
        padding = 0

        # Define our canvas.
        d_height = painter.device().height() - (padding * 2)
        d_width = painter.device().width() - (padding * 2)

        # Draw the bars.
        max_bars = 100
        step_size = d_height / max_bars
        bar_height = step_size #* 0.6
        bar_spacer = 0#step_size * 0.4 / 2

        pc = (value - vmin) / (vmax - vmin)
        n_steps_to_draw = int(pc * max_bars)
        #brush.setColor(QtGui.QColor('red'))
        for n in range(n_steps_to_draw):
            if n < max_bars / 4:
                brush.setColor(QtGui.QColor('green'))
            else :
                if n < max_bars / 2:
                    brush.setColor(QtGui.QColor('yellow'))
                else :
                    brush.setColor(QtGui.QColor('red'))
            rect = QtCore.QRect(
                padding,
                int(padding + d_height - ((n+1) * step_size) + bar_spacer),
                d_width,
                int(bar_height + 1)
            )
            painter.fillRect(rect, brush)

        painter.end()

    def _trigger_refresh(self, val, vm):
        #print("OK")
        self.vm = vm
        self.gf = val
        self.update()

class PowerBar(QtWidgets.QWidget):
    """
    Custom Qt Widget to show a power bar and dial.
    Demonstrating compound and custom-drawn widget.
    """

    def __init__(self, steps=5, *args, **kwargs):
        super(PowerBar, self).__init__(*args, **kwargs)

        layout = QtWidgets.QVBoxLayout()
        self._bar = _Bar()
        layout.addWidget(self._bar)


        #self._dial = QtWidgets.QDial()
        #self._dial.valueChanged.connect(
        #   self._bar._trigger_refresh
        #)

        #layout.addWidget(self._dial)
        self.setLayout(layout)

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        ports = serial.tools.list_ports.comports()
        self.serialInst = serial.Serial()

        portsList = []

        # setting title
        self.setWindowTitle("Proiect Microprocesoare")

        print("Available ports:")
        for onePort in ports:
            portsList.append(str(onePort))
            print(" * " + str(onePort))

        val = input("\nSelect Port: COM")

        for x in range(0,len(portsList)):
            if portsList[x].startswith("COM" + str(val)):
                portVar = "COM" + str(val)
                print(portVar)

        self.serialInst.baudrate = input("\nBaud rate: ") #115200
        self.serialInst.port = portVar
        self.serialInst.open()
        print("\nApp started!!!")
        
        self.widget = QtWidgets.QWidget()
        self.setGeometry(100, 100, 600, 500)
        self.graphWidget = pg.PlotWidget()
        self.btn_flame = QtWidgets.QPushButton('Flame Sensor')
        self.btn_flame.pressed.connect(self.onClickFlame)
        self.btn_flame.setDisabled(True)
        self.btn_sound = QtWidgets.QPushButton('Sound Sensor')
        self.btn_sound.pressed.connect(self.onClickSound)
        self.flag = 0
        self.layout = QtWidgets.QGridLayout()
        self.layout.addWidget(self.btn_flame, 0, 0)
        self.layout.addWidget(self.btn_sound, 1, 0)
        self.layout.addWidget(self.graphWidget, 0, 1, 4, 1)
        self.bar = PowerBar()
        self.layout.addWidget(self.bar, 2, 0, 2, 1)
        self.widget.setLayout(self.layout)
        self.setCentralWidget(self.widget)
        
        self.x = list(range(100))  # 100 time points
        #self.y = [randint(0,100) for _ in range(100)]  # 100 data points
        self.y = [int(0) for _ in range(100)]  # 100 data points

        self.graphWidget.setBackground('w')

        pen = pg.mkPen(color=(255, 0, 0))
        self.data_line =  self.graphWidget.plot(self.x, self.y, pen=pen)

        self.timer = QtCore.QTimer()
        self.timer.setInterval(50)
        self.timer.timeout.connect(self.update_plot_data)
        self.timer.start()

    def update_plot_data(self):
        if self.serialInst.in_waiting:
            packet = self.serialInst.readline()
            self.serialInst.write(b'\n')
            #print(packet)
            stream = packet.decode('utf').strip('\n\r')
            stream_as_list = stream.split(',')
            flame_val = float(stream_as_list[0])
            sound_val = float(stream_as_list[1])
            
            self.x = self.x[1:]  # Remove the first y element.
            self.x.append(self.x[-1] + 1)  # Add a new value 1 higher than the last.

            self.y = self.y[1:]  # Remove the first 
            if self.flag == 0:
                self.y.append(flame_val)  # Add a new random value.
                self.bar._bar._trigger_refresh(self.y, 330)
            else:
                self.y.append(sound_val)  # Add a new random value.
                self.bar._bar._trigger_refresh(self.y, 100)

            self.data_line.setData(self.x, self.y)  # Update the data.
            #self.bar.graph_val = self.y
            
    def onClickFlame(self):
        self.btn_flame.setDisabled(True)
        self.btn_sound.setDisabled(False)
        self.y = [int(0) for _ in range(100)]  # 100 data points
        self.flag = 0
    def onClickSound(self):
        self.btn_flame.setDisabled(False)
        self.btn_sound.setDisabled(True)
        self.y = [int(0) for _ in range(100)]  # 100 data points
        self.flag = 1

suppress_qt_warnings()
app = QtWidgets.QApplication(sys.argv)
w = MainWindow()
w.show()
sys.exit(app.exec_())